<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mod_app_model extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
	}

	public function db_get($params = false, $row = false)
	{
		if ($params) {
			if (isset($params['select']) AND !empty($params['select'])) {
	            $this->db->select($params['select']);
			}

			if (isset($params['where']) AND !empty($params['where'])) {
				$this->db->where($params['where']);
			}

			if (isset($params['or_where']) AND !empty($params['or_where'])) {
				$this->db->or_where($params['or_where']);
			}

			# params["order"] = "kolom asc";
			if (isset($params['order']) AND !empty($params['order'])) {
				$this->db->order_by($params["order"]);
			}

	        if (isset($params['limit']) AND !empty($params['limit'])) {
	            $this->db->limit($params["limit"]);
	        }

	        # params["where_in"] = array("kolom"=> array(1,2,3))
	        if (isset($params['where_in']) AND !empty($params['where_in'])) {
	            $this->db->where_in($params["where_in"]["kolom"],$params["where_in"]["value"]);
	        }
			
			return $row ? $this->db->get($params['table'])->row() : $this->db->get($params['table'])->result();
		}
	}

	public function db_insert($params = false)
	{
		return $this->db->insert($params['table'], $params['data']);
	}

	public function db_delete($params = false)
	{
		return $this->db->delete($params['table'], $params['where']);
	}

	public function db_update($params = false)
	{
		return $this->db->update($params['table'], $params['data'], $params['where']);
	}

}

/* End of file Mod_app_model.php */
/* Location: ./application/models/Mod_app_model.php */